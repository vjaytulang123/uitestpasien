import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from "@angular/http";

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { RecipesPage } from '../pages/recipes/recipes';
import { MedicinesPage } from '../pages/medicines/medicines';
import { MedicinePage } from '../pages/medicine/medicine';
import { TabsPage } from '../pages/tabs/tabs';
import { ChatPage } from "../pages/chat/chat";
import { AlarmPage } from "../pages/alarm/alarm";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    RecipesPage,
    MedicinesPage,
    MedicinePage,
    TabsPage,
    ChatPage,
    AlarmPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    RecipesPage,
    MedicinesPage,
    MedicinePage,
    TabsPage,
    ChatPage,
    AlarmPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
